package im.gitter.gitter.models;

import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

public class CreateRoomModel {

    @Nullable
    private Group community;
    @Nullable
    private String roomName;
    @Nullable
    private Repo linkedRepo;
    private boolean isPrivate;
    private boolean orgCanJoin;
    private boolean githubOnly;
    private boolean addBadge;
    @Nullable
    private Listener listener;


    @Nullable
    public Group getCommunity() {
        return community;
    }

    public void setCommunity(@Nullable Group community) {
        this.community = community;

        if (community != null && linkedRepo != null) {
            if (!linkedRepo.getOwner().equals(community.getGithubGroupName())) {
                setLinkedRepo(null);
                setRoomName(null);
            }
        }

        if (!isOrgCanJoinEnabled()) {
            setOrgCanJoin(false);
        }

        if (!isGithubOnlyEnabled()) {
            setGithubOnly(false);
        }

        notifyChange();
    }

    @Nullable
    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(@Nullable String roomName) {
        this.roomName = roomName;

        if (this.roomName != null && this.linkedRepo != null) {
            if (!this.roomName.equals(this.linkedRepo.getName())) {
                this.linkedRepo = null;
            }
        }

        notifyChange();
    }

    @Nullable
    public Repo getLinkedRepo() {
        return linkedRepo;
    }

    public void setLinkedRepo(@Nullable Repo linkedRepo) {
        if (!isAddBadgeEnabled()) {
            addBadge = false;
        }

        if (linkedRepo != null && this.linkedRepo == null) {
            // enabled by default when selecting a new repo
            addBadge = true;
        }

        this.linkedRepo = linkedRepo;

        if (linkedRepo != null) {
            setRoomName(linkedRepo.getName());
            setPrivate(linkedRepo.isPrivate());
        }

        notifyChange();
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
        notifyChange();
    }

    public boolean isOrgCanJoin() {
        return orgCanJoin;
    }

    public void setOrgCanJoin(boolean orgCanJoin) {
        this.orgCanJoin = orgCanJoin;
        notifyChange();
    }

    public boolean isGithubOnly() {
        return githubOnly;
    }

    public void setGithubOnly(boolean githubOnly) {
        this.githubOnly = githubOnly;
        notifyChange();
    }

    public boolean isAddBadge() {
        return addBadge;
    }

    public void setAddBadge(boolean addBadge) {
        this.addBadge = addBadge;
        notifyChange();
    }

    public boolean isPrivateOptionEnabled() {
        return linkedRepo == null || linkedRepo.isPrivate();
    }

    public boolean isPublicOptionEnabled() {
        return linkedRepo == null || !linkedRepo.isPrivate();
    }

    public boolean isOrgCanJoinEnabled() {
        return isPrivate && (community != null && Group.GH_ORG.equals(community.getBackedBy_type()));
    }

    public boolean isGithubOnlyEnabled() {
        if (community != null) {
            String type = community.getBackedBy_type();
            return Group.GH_ORG.equals(type) || Group.GH_REPO.equals(type) || Group.GH_USER.equals(type);
        } else {
            return false;
        }
    }

    public boolean isAddBadgeEnabled() {
        return linkedRepo != null && !linkedRepo.isPrivate();
    }

    public JSONObject toJSON() throws CommunityMissingException, RoomNameMissingException, JSONException {
        if (community == null) {
            throw new CommunityMissingException();
        }
        if (roomName == null) {
            throw new RoomNameMissingException();
        }

        JSONObject security = new JSONObject();
        security.put("security", isPrivate ? "PRIVATE" : "PUBLIC");
        if (linkedRepo != null) {
            security.put("type", Group.GH_REPO);
            security.put("linkPath", linkedRepo.getUri());
        } else if ((orgCanJoin || !isPrivate) && community.getBackedBy_type() != null) {
            security.put("type", community.getBackedBy_type());
            security.put("linkPath", community.getBackedBy_linkPath());
        }

        JSONObject json = new JSONObject();
        json.put("name", roomName);
        json.put("security", security);
        json.put("addBadge", addBadge);

        if (githubOnly) {
            json.put("providers", new String[] { "github" });
        }

        return json;
    }

    public void setListener(@Nullable Listener listener) {
        this.listener = listener;
    }

    private void notifyChange() {
        if (listener != null) {
            listener.onChange(this);
        }
    }

    public interface Listener {
        void onChange(CreateRoomModel model);
    }

    public class CommunityMissingException extends Exception {}
    public class RoomNameMissingException extends Exception {}
}
