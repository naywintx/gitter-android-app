package im.gitter.gitter.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.rahatarmanahmed.cpv.CircularProgressView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import im.gitter.gitter.R;
import im.gitter.gitter.network.Api;
import im.gitter.gitter.BuildConfig;

public class LoginActivity extends AppCompatActivity {

    private WebView webView;
    private CircularProgressView loadingSpinner;
    private String callbackUrl;
    private RequestQueue requestQueue;
    private String authProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("");
        callbackUrl = BuildConfig.oauth_redirect_uri;
        requestQueue = Volley.newRequestQueue(this);
        authProvider = getIntent().getStringExtra("auth_provider");

        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);

        loadingSpinner = (CircularProgressView) findViewById(R.id.loading_spinner);

        CookieSyncManager.createInstance(this);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();

        webView = (WebView) findViewById(R.id.webv);
        webView.clearCache(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.setWebViewClient(webViewClient);

        webView.loadUrl(getAuthorisationUrl());
    }

    @Override
    public void onPause() {
        super.onPause();

        webView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

        webView.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();

        requestQueue.cancelAll(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        webView.loadUrl("about:blank");
        webView.destroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED);
                finish();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    private WebViewClient webViewClient = new WebViewClient() {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (url.startsWith(callbackUrl)) {
                view.stopLoading();
                Uri uri = Uri.parse(url);
                String code = uri.getQueryParameter("code");

                exchangeTokens(code, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        final String accessToken = response.optString("access_token");

                        getUser(accessToken, new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                String id = response.optJSONObject(0).optString("id");

                                Intent resultIntent = new Intent();
                                resultIntent.putExtra("userId", id);
                                resultIntent.putExtra("accessToken", accessToken);

                                setResult(Activity.RESULT_OK, resultIntent);
                                finish();
                            }
                        });
                    }
                });
            }

            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            loadingSpinner.setVisibility(View.GONE);
        }
    };

    private String getAuthorisationUrl() {
        Resources res = getResources();

        return Uri.parse(res.getString(R.string.host))
                .buildUpon()
                .path("/login/oauth/authorize")
                .appendQueryParameter("client_id", BuildConfig.oauth_client_id)
                .appendQueryParameter("redirect_uri", BuildConfig.oauth_redirect_uri)
                .appendQueryParameter("response_type", "code")
                .appendQueryParameter("source", "android_login-login")
                .appendQueryParameter("auth_provider", authProvider)
                .build()
                .toString();
    }

    private void exchangeTokens(String code, Response.Listener<JSONObject> listener) {
        Resources res = getResources();
        final Api api = new Api(this, null);
        JSONObject body = new JSONObject();

        try {
            body.put("client_id", BuildConfig.oauth_client_id);
            body.put("client_secret", BuildConfig.oauth_client_secret);
            body.put("redirect_uri", BuildConfig.oauth_redirect_uri);
            body.put("grant_type", "authorization_code");
            body.put("code", code);

            requestQueue.add(
                    new JsonObjectRequest(
                            Request.Method.POST,
                            res.getString(R.string.host) + "/login/oauth/token",
                            body,
                            listener,
                            createFailureListener()
                    ) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            return api.getHeaders();
                        }
                    }.setRetryPolicy(api.getRetryPolicy())
                            .setTag(this));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getUser(String accessToken, Response.Listener<JSONArray> listener) {
        final Api api = new Api(this, accessToken);

        requestQueue.add(api.createJsonArrayRequest(
                Request.Method.GET,
                "/v1/user",
                listener,
                createFailureListener()
        ).setTag(this));
    }

    private Response.ErrorListener createFailureListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(
                        LoginActivity.this,
                        R.string.network_failed,
                        Toast.LENGTH_SHORT
                ).show();
                Intent resultIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, resultIntent);
                finish();
            }
        };
    }
}
